package bi_helpers

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"time"
)

type Core struct {
	Customer string `yaml:"customer"`
	Url      string `yaml:"url"`
	Auth     string `yaml:"auth"`
}

type Config struct {
	Dbhost     string `yaml:"dbhost"`
	Dbname     string `yaml:"dbname"`
	Dbuser     string `yaml:"dbuser"`
	Dbpassword string `yaml:"dbpassword"`
	Secret     string `yaml:"secret"`
	Timeout    int    `yaml:"timeout"`
	Cores      []Core `yaml:"cores"`
}

var CoreClient = &http.Client{Timeout: 60 * time.Second}

func GetEnv() (string, string) {

	configer := os.Getenv("CONFIGER")
	if configer == "" {
		fmt.Println("CONFIGER environment variable not set")
		panic("CONFIGER environment variable not set")
	}

	key := os.Getenv("KEY")
	if key == "" {
		fmt.Println("KEY environment variable not set")
		panic("KEY environment variable not set")
	}
	return configer, key
}

func GetConfig(config *Config) {
	configer, key := GetEnv()

	client := http.Client{}
	req, err := http.NewRequest("GET", configer+"/config", nil)
	if err != nil {
		panic(err)
	}

	req.Header = http.Header{
		"Content-Type": {"application/json"},
		"X-KEY":        {key},
	}

	res, err := client.Do(req)
	if err != nil {
		panic(err)
	}

	body, err := io.ReadAll(res.Body)
	if err != nil {
		panic(err)
	}

	err = json.Unmarshal(body, &config)
	if err != nil {
		panic(err)
	}
}

func CoreGET(url string, customerKey string, bsauth string) (body []uint8, err error) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		fmt.Printf("Error creating HTTP request: %s", err.Error())
		return nil, err
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("customer-key", customerKey)
	req.Header.Add("bsauth", bsauth)

	r, err := CoreClient.Do(req)
	if err != nil {
		fmt.Printf("ERR:: %s\n", err.Error())
		return nil, err
	}
	b, err := io.ReadAll(r.Body)
	if err != nil {
		fmt.Printf("ERR:: %s\n", err.Error())
		return
	}
	defer r.Body.Close()
	return b, nil
}
